abstract interface Animal {
    fun identify() : String;
}

class Dog() : Animal {
    override fun identify() : String {
        return "Dog"
    }
}

class Cat() : Animal {
    override fun identify() : String {
        return "Dog"
    }
}

class GenericLife() {
    fun acceptAnimal(animal : Animal) {
        animal.identify()
    }
}


fun main() {

    var life = GenericLife()
    life.acceptAnimal(Cat())
}