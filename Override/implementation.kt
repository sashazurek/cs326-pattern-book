fun main() {
    val sq = Square()
    val tri = Triangle()

    sq.draw()
    tri.draw()
}

abstract class Polygon() {
    abstract fun draw() 
}

class Square : Polygon() {
    override fun draw() {
        println("drawing a square")
    }
}

class Triangle : Polygon() {
    override fun draw() {
        println("drawing a triangle")
    }
}