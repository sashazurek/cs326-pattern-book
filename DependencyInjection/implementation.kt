fun main() {
    // Dependency injection allows more generic operation of a class
    // with functions supplied from the injected dependency

    val motorcycle = Motorcycle(Engine())
    motorcycle.start()
    // Notice how this motorcycle operates exactly like the first,
    // but has a different output
    val motorcycle2 = Motorcycle(BetterEngine())
    motorcycle2.start()
}

class Motorcycle(val engine: Engine) {
    fun start() {
        engine.start()
    }
}

open class Engine() {
    open fun start() {
        println("Started, but not cool")
    }
}

class BetterEngine : Engine() {
    override fun start() {
        println("Started, but cool")
    }
}