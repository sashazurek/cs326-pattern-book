abstract class Process() {
    abstract fun Step1();
}

class ProcessVariation : Process() {
    override fun Step1() {
        println("Modified from abstract")
    }
}