interface Chain {
    fun addString(input: String): String
}

class Type1(val store: String, var next: Chain? = null) : Chain {

    override fun addString(input: String): String = input + store
        .let { next?.addString(it) ?: it }
}

class Type2(val store: String, var next: Chain? = null) : Chain {

    override fun addString(input: String): String = input + store
       .let { next?.addString(it) ?: it }
}
fun main() {
val type1 = Type1("Modified by Type1 Handler\n")
val type2 = Type2("Modified by Type2 Handler\n")

type1.next = type2

val type1message =
    type1.addString("Input\n")
println(type1message)

val type2message =
    type2.addString("Input\n")
println(type2message)
}
