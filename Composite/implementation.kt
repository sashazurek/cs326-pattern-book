interface Shape {
    fun draw();
}

class SquareShape() : Shape {
    override fun draw() {
        println("square");
    }
}
class Circle : Shape {
    override fun draw() {
        println("circle")
    }
}

class Composite : Shape {
    private val list = ArrayList<Shape>()

    fun add(shape: Shape) {
        list.add(shape)
    }

    override fun draw() {
        list.forEach {
            it.draw()
        }
    }
}

fun main() {
    // Initialize child shapes
    var circle = Circle()
    var circle2 = Circle()
    var square = SquareShape()
    var square2 = SquareShape()

    // Initialize tier 2 composites
    var comp1 = Composite()
    var comp2  = Composite()

    // Add child shapes to composites
    comp1.add(circle)
    comp2.add(circle2)
    comp1.add(square)
    comp2.add(square2)

    // Create master composite
    var master = Composite()
    master.add(comp1)
    master.add(comp2)

    // Demonstrate functionality
    master.draw()

}