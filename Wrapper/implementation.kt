/*
    Inside this file are classes that reflect Wrapper patterns.file

    A specific entry is made for the Decorator pattern; the objects Wrapper and
    WrapperClient reflect the remaining wrappers.
 */

class Decorator() {
    private var delegate = SomeObject()

    fun newBehavior() {
        // implement new behavior
    }
    fun delegateBehavior() {
        delegate.behavior()
    }
}

object Wrapper() {
    fun sendValue(input : <T>) {
        WrapperClient().set(input)
    }
    fun getValue() : <T> {
        return WrapperClient().get()
    }
}

class WrapperClient() {
    private var value : <T>

    fun set(input : <T>) {
        value = input
    }
    fun get() : <T> {
        return value
    }
}