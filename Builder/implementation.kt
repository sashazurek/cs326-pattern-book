open class Cycle(name : String, color : String, motor : Boolean) {
    var Name : String = name
    var Color : String = color
    var Motor : Boolean = motor 
}

interface CycleBuilder {
    var Motor : Boolean 

    fun build() : Cycle;
}

class MotorcycleBuilder() : CycleBuilder {
    override var Motor : Boolean = false

    override fun build() : Cycle {
        if (Motor == true) {
            return Cycle("Geoffery","Purple",Motor)
        }
        else return Cycle("null","null",Motor)
    } 
}

class Director(builder : CycleBuilder) {
    private var _builder : CycleBuilder = builder

    fun Construct() {
        _builder.Motor = true
    }
}

fun main() {
    var builder = MotorcycleBuilder()
    var director = Director(builder)

    director.Construct()
    var myCycle : Cycle = builder.build()
}