open class Bottle(filler: String) {
    var filledWith = filler
    open fun empty() : Boolean {
        return false
    }
    fun filledWith() {
        println(filledWith)
    }
    
}

// Implementation makes use of related pattern Singleton
// Through Kotlin keyword object -- see Singleton pattern for details
object NullBottle : Bottle("null") {
    override fun empty() : Boolean {
        return true
    }
}

fun main() {

    // As NullBottle extends Bottle, it responds to the same messages
    println(NullBottle.empty())
    NullBottle.filledWith()

    var fullBottle = Bottle("Dr. Pepper")
    println(fullBottle.empty())
    fullBottle.filledWith()

}

fun withoutNull() {
    val nulltest = Bottle("null")

    if (nulltest.filledWith != "null") {
        // do normal behavior with bottle
    } 
    else {
        // do null behavior with passed bottle
    }
        /*
        Without the null object pattern, there must be conditionals before the object is operated on
        to indicate whether the object is a regular object or a null object

        While in a simple illustrative example, it may seem silly, in a larger program these
        conditionals add up and clutter the code.
        */
}