data class Object1(val id: Long, val data: String)
data class Object2(val id: Byte, val data: String)

interface Mass {
    fun getO1() : Object1
    fun getO2() : Object2
}

abstract class Factory {
    abstract fun create(): Mass

    companion object {
        inline fun <reified T : Mass> create(): Factory =
            when (T::class) {
                Mass::class -> DemoFactory()
                else        -> throw IllegalArgumentException()
            }
    }
}

class DemoFactory() : Factory() {
    override fun create() = CreateMass()
}

class CreateMass() : Mass {
    override fun getO1() : Object1 {
        return Object1(132444234,"data")
    }
    override fun getO2() : Object2 {
        return Object2(65,"data")
    }
}

fun main() {
    println(Factory.create<Mass>().create().getO1())
    println(Factory.create<Mass>().create().getO2())
}